package ru.etu.elbowsupport.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.etu.elbowsupport.models.ASESTestResult;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientView {
    private String primaryASA;
    private int primaryBMI;
    private int age;
    private char side;
    private Integer medicalCardId;
    private String diagnosis;
    private String email;
    private List<ASESTestResult> testResultList;
}
