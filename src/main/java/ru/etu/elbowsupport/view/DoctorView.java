package ru.etu.elbowsupport.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DoctorView {
    private Date dateOfBirth;
    private int age;
    private String email;
    private String name;
    private String lastname;
    private String login;
    private String post;
    private String phone;
}
