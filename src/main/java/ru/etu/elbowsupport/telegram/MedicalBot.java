package ru.etu.elbowsupport.telegram;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import ru.etu.elbowsupport.models.ASESTestResult;
import ru.etu.elbowsupport.models.Patient;
import ru.etu.elbowsupport.models.UrgentCall;
import ru.etu.elbowsupport.models.User;
import ru.etu.elbowsupport.repository.UrgentCallRepository;
import ru.etu.elbowsupport.service.*;
import ru.etu.elbowsupport.service.email.EmailServiceImpl;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * Класс содержит настройку Telegram-бота и обработку полученных команд от пользователя
 */
@Component
//@Data
public class MedicalBot extends TelegramLongPollingBot {

    @Autowired
    private EmailServiceImpl emailService;
    @Autowired
    private ASESService asesService;
    @Autowired
    private UserService userService;
    @Autowired
    private UrgentCallRepository urgentCallRepository;

    @Autowired
    private PatientService patientService;
    private static final Logger log = LoggerFactory.getLogger(MedicalBot.class);


    @Override
    public String getBotUsername() {
        return "MedicalSupBot";
    }

    @Override
    public String getBotToken() {
        return "5189418517:AAEItmBDvB26myvXChDhk1krAeL4RQ2_dZc";
    }

    @PostConstruct
    public void registerBot() throws TelegramApiException {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        try {
            telegramBotsApi.registerBot(this);
        } catch (TelegramApiException e) {
        }
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasPollAnswer()) {
//            log.info("answer is: {},  answer option ID {}", update.getPollAnswer(),
//                    update.getPollAnswer().getOptionIds().get(0));
            //LOG.info("Poll get Question {}", update.getPoll().getQuestion());
            log.info("User poll answer Id {}", update.getPollAnswer().getUser().getId());

            ASESTestResult testResult = asesService.getTestResultByChatId(update.getPollAnswer().getUser().getId().toString());
            testResult.setAnsweredQuestions(testResult.getAnsweredQuestions() + 1);

           // TelegramBotService.isPollAnswerReceived = true;
            switch(update.getPollAnswer().getOptionIds().get(0)) {
                case 0:
                    log.info("Plus 0 to Score");
                    testResult.setTotalSum(testResult.getTotalSum() + 0);
                    break;
                case 1:
                    log.info("Plus 2 to Score");
                    testResult.setTotalSum(testResult.getTotalSum() + 2);
                    break;
                case 2:
                    log.info("Plus 3 to Score");
                    testResult.setTotalSum(testResult.getTotalSum() + 3);
                    break;
                case 3:
                    log.info("Plus 5 to Score");
                    testResult.setTotalSum(testResult.getTotalSum() + 5);
//                    testResult.setTotalSum(testResult.getTotalSum() + Integer.parseInt(painLevel));
                    break;
            }
            asesService.saveTestResult(testResult);
            log.info("Answered: {}", asesService.getTestResultByChatId(update.getPollAnswer().getUser().getId().toString()).getAnsweredQuestions());
        }
        if (update.hasMessage() && update.getMessage().hasText()) {
            if (update.getMessage().getText().startsWith("/login")) {
                String credentials = update.getMessage().getText().replace("/login ", "");
                String[] credentialsSplit = credentials.split(" ");
                log.info("Telegram credentials: {}", Arrays.toString(credentialsSplit));
                log.info("Telegram credentials login: {}", credentialsSplit[0]);
                log.info("Telegram credentials password: {}", credentialsSplit[1]);
                User patient = userService.login(credentialsSplit);
                patientService.setChatID(String.valueOf(patient.getUserId()), String.valueOf(update.getMessage().getChatId()));

            } else if (update.getMessage().getText().startsWith("/вызов")) {
                log.info("Поступил срочный вызов");
                String symptoms = update.getMessage().getText().replace("/вызов ", "");
                String[] symptomsSplit = symptoms.split(" ");
                UrgentCall urgentCall = new UrgentCall(symptomsSplit);
                urgentCall.setDateOfCall(LocalDateTime.now());
                Patient patient = (patientService.getPatientByChatId(String.valueOf(update.getMessage().getChatId())));
                urgentCall.setPatient(patient);
                urgentCallRepository.save(urgentCall);
                String message = "Пациент " + patient.getFullName() + " просит принять срочный вызов со следующими жалобами:\n"
                        + Arrays.toString(symptomsSplit);

                emailService.sendSimpleMessage(patient.getDoctor().getEmail(),
                        "Срочный вызов " + patient.getName() + " " + patient.getLastname(), message);
            } else if (update.getMessage().getText().startsWith("/pain")) {
                String painLevel = "";
                if (update.getMessage().getText().startsWith("/pain")) {
                    painLevel = update.getMessage().getText().replace("/pain", "").strip();
                }
                try {
                    Integer.parseInt(painLevel);
                } catch (NumberFormatException e) {
                    SendMessage message = new SendMessage(); // Create a SendMessage object with mandatory fields
                    message.setChatId(update.getMessage().getChatId().toString());
                    message.setText("Пожалуйста, укажите действительный уровень боли (0-10)");
                    execute(message);
                }

                log.info("Pain level: {}", painLevel);
                ASESTestResult testResult = asesService.getTestResultByChatId(update.getMessage().getChatId().toString());
                testResult.setAnsweredQuestions(testResult.getAnsweredQuestions() + 1);
                testResult.setTotalSum(100 -(testResult.getTotalSum() + Integer.parseInt(painLevel) * 5));
                asesService.saveTestResult(testResult);
                if (testResult.getAnsweredQuestions() == 11) {
                    log.info("Notify doctor of answering all questions {}", testResult.getAnsweredQuestions());
                    testResult.setAnsweredQuestions(0);
                    testResult.setCompleted(true);
                    int totalSum =testResult.getTotalSum();
                    if (totalSum < 50) {
                        testResult.setTotalResultText("Неудовлетворительно");
                    } else if (totalSum <= 69) {
                        testResult.setTotalResultText("Удовлетворительно");
                    }else if (totalSum <= 79) {
                        testResult.setTotalResultText("Хорошо");
                    }else {
                        testResult.setTotalResultText("Отлично");
                    }
                    testResult.setDateCompleted(LocalDateTime.now());
//                    LocalDateTime.now().getDayOfWeek().get
//                    testResult.getDateCompleted().getDayOfWeek()
                    asesService.saveTestResult(testResult);
                    Patient patient = testResult.getPatient();
                    //TODO ИДобавить наименование анкеты, когда сделаю OES
                    String message = "Ваш пациент завершил заполнение анкеты с общим баллом:  " + testResult.getTotalSum() + " " + testResult.getTotalResultText();
                    emailService.sendSimpleMessage(patient.getDoctor().getEmail(),
                            "Пациент " + patient.getName() + " " + patient.getLastname(), message);
                }

            }

        }
    }

}
