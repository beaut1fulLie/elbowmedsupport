package ru.etu.elbowsupport.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;
import ru.etu.elbowsupport.models.ASESTestResult;
import ru.etu.elbowsupport.models.Doctor;
import ru.etu.elbowsupport.models.Patient;

@Service
public class ExportPdf {

	public ByteArrayInputStream employeesReport(List<Doctor> doctors) throws DocumentException, IOException {

		String FONT_FILENAME = "./src/main/resources/arial.ttf";

		BaseFont bf=BaseFont.createFont(FONT_FILENAME, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		Font font=new Font(bf,14,Font.NORMAL);

		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {

			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(80);
			table.setWidths(new int[] { 4, 4, 4 });

			Font headFont = FontFactory.getFont(FontFactory.TIMES_ROMAN);

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Имя", font));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Address", font));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Email", font));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			for (Doctor doctor : doctors) {

				PdfPCell cell;

				cell = new PdfPCell(new Phrase(doctor.getName(), font));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(doctor.getLastname(), font));
				cell.setPaddingLeft(5);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(doctor.getEmail(), font));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPaddingRight(5);
				table.addCell(cell);
			}

			PdfWriter.getInstance(document, out);
			document.open();
			document.add(table);

			document.close();

		} catch (DocumentException ex) {

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	public ByteArrayInputStream asesTestReport(Patient patientInfo) throws DocumentException, IOException {

		String FONT_FILENAME = "./src/main/resources/arial.ttf";

		BaseFont bf=BaseFont.createFont(FONT_FILENAME, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		Font font=new Font(bf,14,Font.NORMAL);

		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {


			Paragraph patientNameParagraph = new Paragraph("Пациент: " + patientInfo.getFullName(), font);
			patientNameParagraph.setAlignment(Element.ALIGN_LEFT);
			Paragraph patientDiagnosisParagraph = new Paragraph("Диагноз:  " + patientInfo.getDiagnosis(), font);
			patientDiagnosisParagraph.setAlignment(Element.ALIGN_LEFT);
			Paragraph resultsTableNameParagraph = new Paragraph("Список пройденных анкет", font);
			resultsTableNameParagraph.setAlignment(Element.ALIGN_CENTER);
			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(80);
			table.setWidths(new int[] { 4, 4, 4 });

			Font headFont = FontFactory.getFont(FontFactory.TIMES_ROMAN);

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Дата прохождения", font));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Сумма баллов", font));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Итог", font));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			for (ASESTestResult testResult : patientInfo.getResults()) {

				PdfPCell cell;

				cell = new PdfPCell(new Phrase(String.valueOf(testResult.getDateSent()
						.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(new Locale("ru")))), font));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(String.valueOf(testResult.getTotalSum()), font));
				cell.setPaddingLeft(5);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				cell = new PdfPCell(new Phrase(testResult.getTotalResultText(), font));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPaddingRight(5);
				table.addCell(cell);
			}

			PdfWriter.getInstance(document, out);
			document.open();
			document.add(patientNameParagraph);
			document.add(new Paragraph(" "));
			document.add(patientDiagnosisParagraph);
			document.add(new Paragraph(" "));
			document.add(resultsTableNameParagraph);
			document.add(new Paragraph(" "));
			document.add(table);


			document.close();

		} catch (DocumentException ex) {

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

}