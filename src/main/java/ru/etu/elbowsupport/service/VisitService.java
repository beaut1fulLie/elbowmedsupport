package ru.etu.elbowsupport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.etu.elbowsupport.enums.ActivityStatus;
import ru.etu.elbowsupport.models.Training;
import ru.etu.elbowsupport.models.Visit;
import ru.etu.elbowsupport.repository.TrainingRepository;
import ru.etu.elbowsupport.repository.VisitRepository;

@Service
public class VisitService {

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private PatientService patientService;

    public Visit save(Visit visit, String patientId) {
        visit.setStatus(ActivityStatus.NEXT);
        visit.setPatient( patientService.getPatientById(patientId));
        return visitRepository.save(visit);
    }
}
