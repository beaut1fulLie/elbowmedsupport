//package ru.etu.elbowsupport.service;
//
//
//import lombok.RequiredArgsConstructor;
//import lombok.extern.log4j.Log4j2;
//import org.springframework.stereotype.Service;
//import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
//import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
//import ru.etu.elbowsupport.models.TelegramPollMessage;
//import ru.etu.elbowsupport.telegram.MedicalBot;
//
//@Service
//@Log4j2
//@RequiredArgsConstructor
//public class RabbitListenerService {
//
//    @RabbitListener(queues = "poll-queue")
//    public void sendPollMessage(TelegramPollMessage telegramPollMessage) throws TelegramApiException {
//        try {
//            SendPoll poll = new SendPoll();
//            poll.setIsAnonymous(telegramPollMessage.isAnonymous);
//            poll.setOptions(telegramPollMessage.options);
//            poll.setQuestion(telegramPollMessage.question);
//            poll.setChatId(telegramPollMessage.chatId);
//            new MedicalBot().execute(poll);
//        }
//        catch (TelegramApiException e) {
//            log.error("Could not send Poll", e);
//        }
//    }
//}
