package ru.etu.elbowsupport.service;

import org.springframework.stereotype.Service;
import ru.etu.elbowsupport.models.Medicine;
import ru.etu.elbowsupport.repository.MedicineRepository;

import java.util.List;
import java.util.UUID;

@Service
public class MedicineService {
    private final MedicineRepository medicineRepository;

    public MedicineService(MedicineRepository medicineRepository) {
        this.medicineRepository = medicineRepository;
    }
    public Medicine register(Medicine medicine) {
        return medicineRepository.save(medicine);
    }

    //TODO Имеет ли смысл?
//    public Medicine update(String id, Medicine medicine) {
//       return  medicineRepository.save(medicine);
//    }
    public void delete(String medicineId) {
        medicineRepository.delete(medicineRepository.findById(UUID.fromString(medicineId)).get());
    }

    public Medicine getMedicineById(String id) {
        return medicineRepository.findById(UUID.fromString(id)).get();
    }
    public List<Medicine> getAllMedicines() {
        return medicineRepository.findAll();
    }
}
