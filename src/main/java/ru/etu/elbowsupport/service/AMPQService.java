//package ru.etu.elbowsupport.service;
//
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.core.AmqpTemplate;
//import org.springframework.stereotype.Service;
//import ru.etu.elbowsupport.configuration.AMPQConfiguration;
//import ru.etu.elbowsupport.models.TelegramPollMessage;
//
//@Service
//@RequiredArgsConstructor
//@Slf4j
//public class AMPQService {
//    private final AmqpTemplate template;
//
//    public void sendTelegramPoll(TelegramPollMessage message) {
//        template.convertAndSend(AMPQConfiguration.queueName, message);
//    }
//}
