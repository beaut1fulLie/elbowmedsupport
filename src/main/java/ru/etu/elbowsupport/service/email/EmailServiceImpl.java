package ru.etu.elbowsupport.service.email;

import com.itextpdf.text.DocumentException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import ru.etu.elbowsupport.models.EmailBasicHeaders;
import ru.etu.elbowsupport.service.DoctorService;
import ru.etu.elbowsupport.service.ExportPdf;
import ru.etu.elbowsupport.service.PatientService;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;

/**
 * Класс содержит логику для отправки писем с помощью Spring Mail
 */
@Component
@Log4j2
public class EmailServiceImpl {

    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private ExportPdf exportPdf;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private PatientService patientService;

    public void sendSimpleMessage(
            String to, String subject, String text) throws MessagingException, DocumentException, IOException {
        MimeMessage mimeMessage = this.emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        MimeMailMessage mimeMailMessage = new MimeMailMessage(helper);
        SimpleMailMessage message = new SimpleMailMessage();
        byte[] bytes = exportPdf.employeesReport(doctorService.getAllDoctors()).readAllBytes();
        DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
        MimeBodyPart pdfBodyPart = new MimeBodyPart();
        pdfBodyPart.setDataHandler(new DataHandler(dataSource));
        pdfBodyPart.setFileName("PatientSummary.pdf");
        MimeMultipart mimeMultipart = new MimeMultipart();
        MimeBodyPart textBodyPart = new MimeBodyPart();
        textBodyPart.setText(text);
        mimeMultipart.addBodyPart(textBodyPart);
        mimeMultipart.addBodyPart(pdfBodyPart);
        mimeMessage.setContent(mimeMultipart);
        mimeMessage.setRecipients(Message.RecipientType.TO, to);
        log.info("Recipient email: {}", (Object) mimeMessage.getRecipients(Message.RecipientType.TO));
        message.setFrom("noreplyelbowsup@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(mimeMessage);
    }
    public void sendSimpleMessageWithASESTest(String patientId, EmailBasicHeaders emailBasicHeaders) throws MessagingException, DocumentException, IOException {
        MimeMessage mimeMessage = this.emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        MimeMailMessage mimeMailMessage = new MimeMailMessage(helper);
        SimpleMailMessage message = new SimpleMailMessage();
        byte[] bytes = exportPdf.asesTestReport(patientService.getPatientById(patientId)).readAllBytes();
        DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
        MimeBodyPart pdfBodyPart = new MimeBodyPart();
        pdfBodyPart.setDataHandler(new DataHandler(dataSource));
        pdfBodyPart.setFileName("PatientASESTests.pdf");
        MimeMultipart mimeMultipart = new MimeMultipart();
        MimeBodyPart textBodyPart = new MimeBodyPart();
        mimeMessage.setSubject(emailBasicHeaders.getSubject());
        textBodyPart.setText(emailBasicHeaders.getText());
        mimeMultipart.addBodyPart(textBodyPart);
        mimeMultipart.addBodyPart(pdfBodyPart);
        mimeMessage.setContent(mimeMultipart);
        mimeMessage.setRecipients(Message.RecipientType.TO, emailBasicHeaders.getTo());
        log.info("Recipient email: {}", (Object) mimeMessage.getRecipients(Message.RecipientType.TO));
        message.setFrom("noreplyelbowsup@gmail.com");
        emailSender.send(mimeMessage);
    }
}