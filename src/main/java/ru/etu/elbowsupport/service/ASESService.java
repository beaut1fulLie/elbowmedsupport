package ru.etu.elbowsupport.service;

import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.etu.elbowsupport.models.ASESQuestion;
import ru.etu.elbowsupport.models.ASESTestResult;
import ru.etu.elbowsupport.models.EmailBasicHeaders;
import ru.etu.elbowsupport.models.Patient;
import ru.etu.elbowsupport.repository.ASESQuestionRepository;
import ru.etu.elbowsupport.repository.ASESTestResultRepository;
import ru.etu.elbowsupport.service.email.EmailServiceImpl;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Данный класс содержит бизнес-логику обработки, создания и отправки анкеты ASES
 */
@Service
public class ASESService {

    @Autowired
    private PatientService patientService;
    @Autowired
    private ASESQuestionRepository asesQuestionRepository;
    @Autowired
    private ASESTestResultRepository testResultRepository;
    @Autowired
    private EmailServiceImpl emailService;

    public ASESQuestion saveQuestion(ASESQuestion question) {
        return asesQuestionRepository.save(question);
    }

    public ASESTestResult saveTestResult(ASESTestResult testResult) {
        return testResultRepository.save(testResult);
    }


    /**
     * Получение результата анкетирования по идентификатору чата в Telegram
     * @param chatId Идентификатор Telegram-чата
     * @return
     */
    //С учетом того, что пациент обязательно заполняет анкету, до отправки следующей
    public ASESTestResult getTestResultByChatId(String chatId) {
        return testResultRepository.findASESTestResultByIsCompletedAndPatient(false, patientService.getPatientByChatId(chatId));
    }


    public ASESTestResult saveTestResultByChatId(String chatId) {
        Patient patient = patientService.getPatientByChatId(chatId);
        ASESTestResult testResult = new ASESTestResult();
        testResult.setPatient(patient);
        return testResultRepository.save(testResult);
    }

    public List<ASESQuestion> saveQuestions(List<String> questions) {
        List<ASESQuestion> asesQuestions = new ArrayList<>();
        for (String question : questions) {
            ASESQuestion asesQuestion = new ASESQuestion();
            asesQuestion.setQuestion(question);
            asesQuestionRepository.save(asesQuestion);
            asesQuestions.add(asesQuestion);
        }
        return asesQuestions;
    }
    public List<ASESTestResult> getResultsByPatient(String patientId) {
        return testResultRepository.findASESTestResultsByPatient(patientService.getPatientById(patientId));
    }

    public void sendASESTestEmail(EmailBasicHeaders basicHeaders) throws MessagingException, DocumentException, IOException {
        emailService.sendSimpleMessage(basicHeaders.getTo(), basicHeaders.getSubject(), basicHeaders.getText());
    }
}
