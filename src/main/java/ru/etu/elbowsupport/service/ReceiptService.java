package ru.etu.elbowsupport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.etu.elbowsupport.models.Receipt;
import ru.etu.elbowsupport.models.Receipt;
import ru.etu.elbowsupport.repository.ReceiptRepository;

import java.util.List;
import java.util.UUID;

@Service
public class ReceiptService {
    private final ReceiptRepository receiptRepository;

    @Autowired
    public ReceiptService(ReceiptRepository receiptRepository) {
        this.receiptRepository = receiptRepository;
    }


    @Transactional
    public Receipt saveTest() {
        Receipt testReceipt = new Receipt();
        receiptRepository.save(testReceipt);
        return testReceipt;
    }


    public Receipt register(Receipt receipt) {
        return receiptRepository.save(receipt);
    }
    
    //TODO Имеет ли смысл?
//    public Receipt update(String id, Receipt receipt) {
//       return  receiptRepository.save(receipt);
//    }
    public void delete(String receiptId) {
        receiptRepository.delete(receiptRepository.findById(UUID.fromString(receiptId)).get());
    }

    public Receipt getReceiptById(String id) {
        return receiptRepository.findById(UUID.fromString(id)).get();
    }
    public List<Receipt> getAllReceipts() {
        return receiptRepository.findAll();
    }
}
