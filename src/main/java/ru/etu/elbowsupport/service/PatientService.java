package ru.etu.elbowsupport.service;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.etu.elbowsupport.enums.RolesEnum;
import ru.etu.elbowsupport.models.*;
import ru.etu.elbowsupport.models.Patient;
import ru.etu.elbowsupport.repository.PatientRepository;
import ru.etu.elbowsupport.repository.RoleRepository;
import ru.etu.elbowsupport.view.PatientView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@NoArgsConstructor
@Slf4j
public class PatientService {
    private PatientRepository patientRepository;
    private UserService userService;
    private DoctorService doctorService;
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, UserService userService, DoctorService doctorService) {
        this.patientRepository = patientRepository;
        this.userService = userService;
        this.doctorService = doctorService;
    }
    @Transactional
    public Patient save(Patient patient, String doctorID) {
        log.info("Patient: {}", patient.toString());
        Doctor doctor = doctorService.getDoctorById(doctorID);
        userService.register(patient);
        List<ASESTestResult> results = new ArrayList<>();
        List<Patient> patients = doctor.getPatients();
        patient.setDoctor(doctor);
        patient.setResults(results);
        patients.add(patient);
        doctorService.save(doctor);
        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.findByTitle(RolesEnum.valueOf("ROLE_USER")).get());
        roles.add(roleRepository.findByTitle(RolesEnum.valueOf("ROLE_PATIENT")).get());
        patient.setRoles(roles);
        permissionService.addPermissionForUser(patient, BasePermission.ADMINISTRATION, doctor.getUsername());
        Patient savedPatient = patientRepository.save(patient);
        permissionService.addPermissionForUser(doctor, BasePermission.READ, savedPatient.getUsername());

        return savedPatient;

    }
    public Patient login(Patient patient) {
        return (Patient) userService.login(patient);
    }


    public Patient save(Patient patient) {
        log.info("Patient: {}", patient.toString());
        userService.register(patient);
        return patientRepository.save(patient);

    }

    @Deprecated
    public Patient register(Patient patient) {
        return patientRepository.save(patient);
    }

    public void delete(String patientId) {
        patientRepository.delete(patientRepository.findById(UUID.fromString(patientId)).get());
    }
    public Patient update(String id, PatientView patientView) {
        Patient patient = patientRepository.getById(UUID.fromString(id));
        patient.setPrimaryASA(patientView.getPrimaryASA());
        patient.setPrimaryBMI(patientView.getPrimaryBMI());
        patient.setAge(patientView.getAge());
        patient.setSide(patientView.getSide());
        patient.setMedicalCardId(patientView.getMedicalCardId());
        patient.setDiagnosis(patientView.getDiagnosis());
        patient.setEmail(patientView.getEmail());
        return patientRepository.save(patient);
    }

    public Patient getPatientById(String id) {
        return patientRepository.findById(UUID.fromString(id)).get();
    }
    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    public Patient setChatID(String id,String chatID) {
        Patient patient = getPatientById(id);
        patient.setTelegramChatId(chatID);
        return patientRepository.save(patient);
    }
    public Patient setTrainings(String id,List<Training> trainings) {
        Patient patient = getPatientById(id);
        patient.setTrainings(trainings);
        return patientRepository.save(patient);
    }

    public Doctor getDoctor(String patientId) {
        return getPatientById(patientId).getDoctor();
    }

    public List<ASESTestResult> getPatientResults(String patientId) {
        return getPatientById(patientId).getResults();
    }

    public Patient getPatientByChatId(String chatId) {
        return patientRepository.findByTelegramChatId(chatId);
    }
}
