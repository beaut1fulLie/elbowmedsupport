package ru.etu.elbowsupport.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.etu.elbowsupport.models.ASESTestResult;
import ru.etu.elbowsupport.models.Patient;
import ru.etu.elbowsupport.models.TelegramPollMessage;
import ru.etu.elbowsupport.telegram.MedicalBot;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс содержит бизнес-логику отправки анкет ASES пациентам
 */
@Service
@EnableScheduling
@Log4j2
public class TelegramBotService {

    private final ASESService asesService;
    private final ReceiptService receiptService;
    private final PatientService patientService;
    //private final AMPQService ampqService;
    @Value("${app.nikitaChatId}")
    private String chatId;

    private String pollId;

    public static volatile boolean isPollAnswerReceived;



    // TODO add questions to DB
    //List of ASES questions and answers
    // --------------------------------------------------------------------------------------------

    private List<String> questions = new ArrayList<>() {
        {
            add("Трудно ли Вам надевать пальто?");
            add("Трудно ли Вам спать на больной сторон?");
            add("Трудно ли Вам мыть спину самостоятельно / застегивать бюстгальтер?");

            add("Трудно ли Вам пользоваться туалет?");
            add("Трудно ли Вам причесывать волосы?");
            add("Трудно ли Вам дотянуться до высокой полки?");
            add(" Трудно ли Вам поднять груз весом 4,5 кг выше уровня плечевого сустава?");
            add("Трудно ли Вам бросать мяч из-за головы?");
            add("Трудно ли Вам выполнять обычную для Вас работу?");
            add("Трудно ли Вам заниматься обычным для Вас видом спорта (активным отдыхом)?");
        }
    };
    private List<String> answers = new ArrayList<>() {
        {
            add("Не трудно");
            add("Затруднительно");
            add("Очень трудно");
            add("Невозможно");
        }
    };

    public TelegramBotService(ASESService asesService, ReceiptService receiptService, PatientService patientService) {
        this.asesService = asesService;
        this.receiptService = receiptService;
        //this.ampqService = ampqService;
        asesService.saveQuestions(questions);
        this.patientService = patientService;
    }
    // --------------------------------------------------------------------------------------------

    public void sendAsesPoll() throws TelegramApiException {
        log.info("Chat id {}", chatId);
        for (int i = 0; i < questions.size(); i++) {
            isPollAnswerReceived = false;
            //TelegramPollMessage pollMessage = new TelegramPollMessage(chatId, answers, questions.get(i), null);

            SendPoll poll = new SendPoll();

            poll.setIsAnonymous(false);
            poll.setChatId(chatId);
            poll.setQuestion(questions.get(i));
            poll.setOptions(answers);

            new MedicalBot().execute(poll);
            //ampqService.sendTelegramPoll(pollMessage);
            log.info("Poll to String {}", poll.toString());
        }
    }

    @Transactional
    public void sendAsesPoll(String patientId) throws TelegramApiException {
        Patient patient = patientService.getPatientById(patientId);
        String id = patient.getTelegramChatId();
        List<ASESTestResult> results = patient.getResults();
        results.add(asesService.saveTestResultByChatId(id));
        patient.setResults(results);
        patientService.save(patient);
        log.info("Chat id {}", id);
        for (int i = 0; i < questions.size(); i++) {
            isPollAnswerReceived = false;
            SendPoll poll = new SendPoll();

            poll.setIsAnonymous(false);
            poll.setChatId(id);
            poll.setQuestion(questions.get(i));
            poll.setOptions(answers);

            new MedicalBot().execute(poll);
            log.info("Poll to String {}", poll.toString());
        }
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(id);
        sendMessage.setText("Пожалуйста, укажите интенсивность боли от 0 до 10, где 10 - самая сильная боль(пример /pain 5) ");
        new MedicalBot().execute(sendMessage);
    }


    //TODO Довольно затратный способ
    //CRON - s m h d m DoW
    //CRON - 0 0 15 * * * Every day at 15:00
    @Transactional
    @Scheduled(cron="0 0 15 * * *")
    public void sendAsesPollScheduled() throws TelegramApiException {


        log.info("Started scheduled operation");
        for (Patient patient : patientService.getAllPatients()) {
//            patientObj.getTelegramChatId()
//            Patient patient = patientService.getPatientById(patientId);
            if (!patient.getResults().isEmpty()) {
                log.info(patient.getResults());
                DayOfWeek startDate = patient.getResults().get(0).getDateSent().getDayOfWeek();
                if (startDate.equals(LocalDateTime.now().getDayOfWeek())) {
                    log.info("Started scheduled operation for Patient: {}", patient.getFullName());
                    String id = patient.getTelegramChatId();
                    List<ASESTestResult> results = patient.getResults();
                    results.add(asesService.saveTestResultByChatId(id));
                    patient.setResults(results);
                    patientService.save(patient);
                    log.info("Chat id {}", id);
                    for (int i = 0; i < questions.size(); i++) {
                        isPollAnswerReceived = false;
                        SendPoll poll = new SendPoll();

                        poll.setIsAnonymous(false);
                        poll.setChatId(id);
                        poll.setQuestion(questions.get(i));
                        poll.setOptions(answers);

                        new MedicalBot().execute(poll);
                        log.info("Poll to String {}", poll.toString());
                    }
                    SendMessage sendMessage = new SendMessage();
                    sendMessage.setChatId(id);
                    sendMessage.setText("Пожалуйста, укажите интенсивность боли от 0 до 10, где 10 - самая сильная боль(пример /pain 5) ");
                    new MedicalBot().execute(sendMessage);
                }
            }
        }
    }
}
