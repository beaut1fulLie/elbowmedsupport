package ru.etu.elbowsupport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.etu.elbowsupport.enums.RolesEnum;
import ru.etu.elbowsupport.models.Doctor;
import ru.etu.elbowsupport.models.Patient;
import ru.etu.elbowsupport.models.Role;
import ru.etu.elbowsupport.models.User;
import ru.etu.elbowsupport.repository.DoctorRepository;
import ru.etu.elbowsupport.repository.RoleRepository;
import ru.etu.elbowsupport.view.DoctorView;
import ru.etu.elbowsupport.view.PatientView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class DoctorService {
    private final DoctorRepository doctorRepository;
    private final UserService userService;
    private final RoleRepository roleRepository;
    @Autowired
    private PermissionService permissionService;

    public DoctorService(DoctorRepository doctorRepository, UserService userService, RoleRepository roleRepository) {
        this.doctorRepository = doctorRepository;
        this.userService = userService;
        this.roleRepository = roleRepository;
    }

    @Transactional
    public Doctor register(Doctor doctor) {
        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.findByTitle(RolesEnum.valueOf("ROLE_USER")).get());
        roles.add(roleRepository.findByTitle(RolesEnum.valueOf("ROLE_DOCTOR")).get());
        doctor.setRoles(roles);
        Doctor doc = doctorRepository.save(doctor);

        userService.register(doctor);
        permissionService.addPermissionForUser(doc, BasePermission.ADMINISTRATION, doc.getUsername());
        return doc;
    }
    public Doctor save(Doctor doctor) {
        return doctorRepository.save(doctor);
    }

    public Doctor login(Doctor doctor) {
        return (Doctor)userService.login(doctor);
    }
    public void delete(String doctorId) {
        doctorRepository.delete(doctorRepository.findById(UUID.fromString(doctorId)).get());
    }

    public Doctor getDoctorById(String id) {
        return doctorRepository.findById(UUID.fromString(id)).get();
    }
    public Doctor getDoctorByLogin(String login) {
        return doctorRepository.findByLogin(login).get();
    }
    public List<Doctor> getAllDoctors() {
        return doctorRepository.findAll();
    }

    public List<Patient> getAllPatientOfDoctor(String doctorId) {
        Doctor doctor = doctorRepository.getById(UUID.fromString(doctorId));
        return doctor.getPatients();
    }

    @Transactional
    public Doctor update(String id, DoctorView doctor) {
        Doctor updateDoctor = doctorRepository.getById(UUID.fromString(id));
        updateDoctor.setAge(doctor.getAge());
        updateDoctor.setPost(doctor.getPost());
        updateDoctor.setLastname(doctor.getLastname());
        updateDoctor.setName(doctor.getName());
        updateDoctor.setEmail(doctor.getEmail());
        updateDoctor.setPhone(doctor.getPhone());
        return doctorRepository.save(updateDoctor);
    }
    public Doctor update(Doctor doctor) {
        //Doctor updateDoctor = doctorRepository.getById(UUID.fromString(id));
        return doctorRepository.save(doctor);
    }
}
