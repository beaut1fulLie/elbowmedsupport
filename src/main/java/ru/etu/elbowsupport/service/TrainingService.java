package ru.etu.elbowsupport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.etu.elbowsupport.models.Patient;
import ru.etu.elbowsupport.models.Training;
import ru.etu.elbowsupport.repository.TrainingRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TrainingService {

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private PatientService patientService;

    public Training save(Training training, String patientId) {
        Patient patient = patientService.getPatientById(patientId);
        List<Training> trainings = patient.getTrainings();
        training.setTrainingDate(LocalDateTime.now());
        training.setDescription("Какие-то упражнения");
        training.setTotalExercise(10);
        trainings.add(training);

        patientService.setTrainings(patientId,trainings);
        training.setPatient(patient);
        return trainingRepository.save(training);
    }
}
