package ru.etu.elbowsupport.service;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.etu.elbowsupport.models.User;
import ru.etu.elbowsupport.repository.UserRepository;

import java.util.Optional;

@Service
@Log4j2
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

//    public boolean login(String login, String password) {
//        Optional<User> user = userRepository.findByLogin(login);
//        user.ifPresent(value -> value.getPassword().equals(password));
//    }
//
    public User register(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(user.getRoles());
       return userRepository.save(user);
    }
    public User save(User user) {
        return userRepository.save(user);
    }

    public User login(User user) {

        User checkUser = userRepository.findByLogin(user.getLogin()).get();
        log.info("Login Patient {}", checkUser);
        if (bCryptPasswordEncoder.matches(user.getPassword(), checkUser.getPassword())) {
            return checkUser;
        } else return null;
    }

    public User login(String[] credentials) {

        User checkUser = userRepository.findByLogin(credentials[0]).get();
        log.info("Login Patient {}", checkUser);
        if (bCryptPasswordEncoder.matches(credentials[1], checkUser.getPassword())) {
            return checkUser;
        } else return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("LoadByUsername: login {}", username);
        Optional<User> user = userRepository.findByLogin(username);
        return user.get();
    }
}
