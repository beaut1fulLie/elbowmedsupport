package ru.etu.elbowsupport.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.*;
import org.springframework.stereotype.Service;
import ru.etu.elbowsupport.models.Identifiable;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PermissionService {
    private final MutableAclService aclService;

    public void addPermissionForUser(Identifiable targetObj, Permission permission, String username) {
        final Sid sid = new PrincipalSid(username);
        addPermissionForSid(targetObj, permission, sid);
    }

    public void addPermissionForAuthority(Identifiable targetObj, Permission permission, String authority) {
        final Sid sid = new GrantedAuthoritySid(authority);
        addPermissionForSid(targetObj, permission, sid);
    }

    public void delPermissionForUser(Identifiable targetObj, Permission permission, String username) {
        final Sid sid = new PrincipalSid(username);
        delPermissionForSid(targetObj, permission, sid);
    }

    public void delPermissionForAuthority(Identifiable targetObj, Permission permission, String authority) {
        final Sid sid = new GrantedAuthoritySid(authority);
        delPermissionForSid(targetObj, permission, sid);
    }

    private void addPermissionForSid(Identifiable targetObj, Permission permission, Sid sid) {
        MutableAcl acl = getAclByTarget(targetObj);

        acl.insertAce(acl.getEntries().size(), permission, sid, true);
        aclService.updateAcl(acl);
    }

    private void delPermissionForSid(Identifiable targetObj, Permission permission, Sid sid) {
        MutableAcl acl = getAclByTarget(targetObj);

        List<AccessControlEntry> entries = acl.getEntries();
        Optional<AccessControlEntry> first = entries.stream().filter(ace -> (ace.getPermission().equals(permission) && ace.getSid().equals(sid))).findFirst();

        if(!first.isPresent()) return;

        int i = entries.indexOf(first.get());

        acl.deleteAce(i);

        aclService.updateAcl(acl);
    }

    private MutableAcl getAclByTarget(Identifiable identifiable){
        final ObjectIdentity oi = new ObjectIdentityImpl(identifiable.getClass(), identifiable.getId());
        try {
            return  (MutableAcl) aclService.readAclById(oi);
        } catch (final NotFoundException nfe) {
            return aclService.createAcl(oi);
        }
    }
}
