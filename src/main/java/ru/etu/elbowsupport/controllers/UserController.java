package ru.etu.elbowsupport.controllers;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.etu.elbowsupport.configuration.security.AuthenticationFacade;
import ru.etu.elbowsupport.enums.RolesEnum;
import ru.etu.elbowsupport.models.Doctor;
import ru.etu.elbowsupport.models.Role;
import ru.etu.elbowsupport.models.User;
import ru.etu.elbowsupport.repository.RoleRepository;
import ru.etu.elbowsupport.service.UserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "users")
@Log4j2
public class UserController {


    @Autowired
    UserService userService;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AuthenticationFacade authenticationFacade;

    @GetMapping(path = "/user")
    public User getCurrentUser() {
        log.info("Get current user");
        return authenticationFacade.getPrincipal();
    }
    @GetMapping(path = "/users")
    public List<User> allUsers() {
        List<User> allUsers = new ArrayList<>();
        allUsers.add(new User("Frank","Lee", 15));
        allUsers.add(new User("Bob","Biba", 15));
        return allUsers;
        //
    }

    @PostMapping(path = "/addAdmin")
    public User register(@RequestBody User user) {
        User admin = new User();
        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.findByTitle(RolesEnum.valueOf("ROLE_USER")).get());
        roles.add(roleRepository.findByTitle(RolesEnum.valueOf("ROLE_ADMIN")).get());
        user.setRoles(roles);

        return userService.register(user);
    }

    @PostMapping(path = "loginAsAdmin")
    public User login(@RequestBody User user) {
        return userService.login(user);
    }

}
