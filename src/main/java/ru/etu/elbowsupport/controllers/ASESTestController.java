package ru.etu.elbowsupport.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.etu.elbowsupport.models.ASESTestResult;
import ru.etu.elbowsupport.service.ASESService;
import ru.etu.elbowsupport.service.TelegramBotService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/testResults")
public class ASESTestController {

    @Autowired
    private ASESService asesService;

    @Autowired
    private TelegramBotService telegramBotService;

    @GetMapping(path = "/{id}")
    public List<ASESTestResult> getAllResultsOfPatient(@PathVariable UUID id) {
        return asesService.getResultsByPatient(String.valueOf(id));
    }
//
//    @GetMapping(path = "/{id}")
//    public List<ASESTestResult> sendASESTest(@PathVariable UUID id) {
//        return asesService.getResultsByPatient(String.valueOf(id));
//    }
}
