package ru.etu.elbowsupport.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.etu.elbowsupport.models.ASESTestResult;
import ru.etu.elbowsupport.models.Doctor;
import ru.etu.elbowsupport.models.Patient;
import ru.etu.elbowsupport.models.User;
import ru.etu.elbowsupport.service.PatientService;
import ru.etu.elbowsupport.view.PatientView;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "patient")
@Slf4j
public class PatientController {
    @Autowired
    private PatientService patientService;
    public List<Patient> getAllPatients() {
        return patientService.getAllPatients();
    }


    @PostMapping(path = "login")
    public Patient login(@RequestBody Patient patient) {
        Patient user = patientService.login(patient);
        log.info(user.toString());

        return user;
    }
    @PostMapping(path = "create/{doctorId}")
    public Patient savePatient(@RequestBody Patient patient, @PathVariable String doctorId) {
        return patientService.save(patient, doctorId);
    }

    @PostMapping(path = "create")
    public Patient savePatientWithoutDoctor(@RequestBody Patient patient) {
        return patientService.save(patient);
    }

    @PostMapping(path = "setChatId/{patientId}")
    public Patient setPatientChatId(@RequestBody String chatID, @PathVariable String patientId) {
        log.info("Chat ID: {}, patient ID: {}", chatID, patientId);
        return patientService.setChatID(patientId, chatID);
    }
    @GetMapping(path="{id}")
    public Patient getPatient(@PathVariable String id) {
        return patientService.getPatientById(id);
    }

    @PreAuthorize("hasPermission(#id, 'ru.etu.elbowsupport.models.Patient', 'ADMINISTRATION')")
    @PutMapping(path="update/{id}")
    public Patient updatePatient(@PathVariable UUID id, @RequestBody PatientView patient) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("User Details principal: {}" , authentication.getPrincipal());
        return patientService.getPatientById(String.valueOf(id));
    }


    //TODO Rework to return DoctorView
    @GetMapping(path = "{patientId}/doctor")
    public Doctor getDoctor(@PathVariable String patientId) {
        return patientService.getDoctor(patientId);
    }

    @GetMapping(path = "{patientId}/results")
    public List<ASESTestResult> getPatientResults(@PathVariable String patientId) {
        return patientService.getPatientResults(patientId);
    }

}
