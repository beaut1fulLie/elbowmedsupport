package ru.etu.elbowsupport.controllers;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.etu.elbowsupport.service.ReceiptService;
import ru.etu.elbowsupport.service.TelegramBotService;
import ru.etu.elbowsupport.telegram.MedicalBot;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "telegram/test")
@Log4j2
public class TelegramTestController {

    @Autowired
    private ReceiptService receiptService;

    @Autowired
    private TelegramBotService telegramBotService;




    @GetMapping(path = "/sendForm")
    public void sendForm() throws TelegramApiException {
        log.info("Send form /sendForm path");
        telegramBotService.sendAsesPoll();
    }
    @GetMapping(path = "/sendFormById/{id}")
    public void sendForm(@PathVariable String id) throws TelegramApiException {
        log.info("Send form /sendForm path");
        telegramBotService.sendAsesPoll(id);
    }
    @GetMapping(path = "/poll")
    public void sendPoll() throws TelegramApiException {
        Message pollMessage = new Message();

        SendPoll poll = new SendPoll();
        poll.setChatId("447224908");
        poll.setQuestion("Did you ever hear the tragedy of Darth Plagueis The Wise?");
        List<String> options = new ArrayList<>();
        options.add("Yes");
        options.add("No");
        options.add("It is not a Jedi way");
        poll.setOptions(options);
        poll.setIsAnonymous(false);
        new MedicalBot().execute(poll);

    }
}
