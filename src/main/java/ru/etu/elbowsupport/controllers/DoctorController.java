package ru.etu.elbowsupport.controllers;

import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.http.parser.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.etu.elbowsupport.configuration.security.AuthenticationFacade;
import ru.etu.elbowsupport.models.Doctor;
import ru.etu.elbowsupport.models.Patient;
import ru.etu.elbowsupport.service.DoctorService;
import ru.etu.elbowsupport.service.PermissionService;
import ru.etu.elbowsupport.service.UserService;
import ru.etu.elbowsupport.view.DoctorView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.http.HttpRequest;
import java.util.List;
import java.util.UUID;

@RestController
//@CrossOrigin
@RequestMapping(path = "doctor")
@Log4j2
public class DoctorController {
    @Autowired
    private DoctorService doctorService;

    @Autowired
    private UserService userService;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private AuthenticationFacade authenticationFacade;


    @PostMapping(path = "register")
    public Doctor register(@RequestBody Doctor doctor) {
        Doctor doc = doctorService.register(doctor);
        log.info(doc.toString());
        return doc;
    }

    @PostMapping(path = "login")
    public Doctor login(@RequestBody Doctor doctor) {
        Doctor doc = doctorService.login(doctor);
        log.info(doc.toString());
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        log.info("Principal {}", auth.getPrincipal());
        return doc;
    }

    @PatchMapping(path = "/{id}/update")
    public Doctor update(@RequestBody DoctorView doctor, @PathVariable UUID id) {
        log.info("Update with Patch Requests");
        Doctor doc = doctorService.update(String.valueOf(id), doctor);
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        log.info("Principal {}", auth.getPrincipal());
        return doc;
    }

//    @PatchMapping(path = "/{id}/update")
//    public Doctor update(@RequestBody Doctor doctor, @PathVariable UUID id) {
//        Doctor doc = doctorService.update(doctor);
////        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
////        log.info("Principal {}", auth.getPrincipal());
//        return doc;
//    }

    @PreAuthorize("hasPermission(#id,'ru.etu.elbowsupport.models.Patient','READ')")
    @GetMapping(path="{id}")
    public Doctor getDoctor(@PathVariable String id) {
        return doctorService.getDoctorById(id);
    }

//    @PutMapping(path="{id}")
//    public Doctor updateDoctor(@PathVariable String id, @RequestBody Doctor doctor) {
//        return doctorService.update(id, doctor);
//    }

    @PreAuthorize("hasPermission(#id, 'ru.etu.elbowsupport.models.Doctor', 'ADMINISTRATION')")
    @GetMapping(path="{id}/patients")
    public List<Patient> getAllPatientsOfDoctor(@PathVariable UUID id) {
        return doctorService.getAllPatientOfDoctor(String.valueOf(id));
    }

    @DeleteMapping(path = "{id}/delete")
    public void deleteDoctor(@PathVariable String id) {
        doctorService.delete(id);
    }


    @GetMapping(path = "all")
    public List<Doctor> getAllDoctors() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("User Details principal: {}" , authentication.getPrincipal());
        return doctorService.getAllDoctors();
    }

    @GetMapping(path="myProfile")
    public Doctor getCurrentDoctor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("User Details principal: {}" , authentication.getPrincipal());
        return doctorService.getDoctorByLogin(String.valueOf(authentication.getPrincipal()));
    }
}
