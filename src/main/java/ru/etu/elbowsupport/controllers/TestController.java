package ru.etu.elbowsupport.controllers;

import com.itextpdf.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.etu.elbowsupport.models.*;
import ru.etu.elbowsupport.repository.DoctorRepository;
import ru.etu.elbowsupport.service.ReceiptService;
import ru.etu.elbowsupport.service.email.EmailServiceImpl;
import ru.etu.elbowsupport.telegram.MedicalBot;

import javax.mail.MessagingException;
import javax.naming.event.ObjectChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(path = "test")
@CrossOrigin("*")

public class TestController {

    @Autowired
    EmailServiceImpl emailService;
//    @Autowired
//    private ReceiptService receiptService;
private static final Logger log = LoggerFactory.getLogger(TestController.class);
    private final ReceiptService receiptService;
    private final DoctorRepository doctorRepository;


    @Autowired
    public TestController(ReceiptService receiptService, DoctorRepository doctorRepository) {
        this.receiptService = receiptService;
        this.doctorRepository = doctorRepository;
    }
    @GetMapping(path = "/hello")
    public User hello() {
        log.info("Hello get");
        return new User("Frank","Lee", 15);
    }
    @GetMapping(path = "/user")
    public User helloUser() {
        return new User("Frank","Lee", 15);
    }
    @GetMapping(path = "/users")
    public List<User> allUsers() {
        List<User> allUsers = new ArrayList<>();
        allUsers.add(new User("Frank","Lee", 15));
        allUsers.add(new User("Bob","Biba", 15));
        return allUsers;
    }
    @GetMapping(path = "/patient")
    public User helloPatient() {
        return new Patient(20,'M', 'L');
    }
    @GetMapping(path = "/patients")
    public List<Patient> allPatients() {
        List<Patient> allUsers = new ArrayList<>();
        allUsers.add(new Patient(29,'M', 'L'));
        allUsers.add(new Patient(20,'M', 'L'));
        return allUsers;
    }

    @GetMapping(path = "/medicine")
    public Medicine helloMedicine() {
        return new Medicine("Syrup");
    }
    @GetMapping(path = "/medicines")
    public List<Medicine> allMedicines() {
        List<Medicine> medicines = new ArrayList<>();
        medicines.add(new Medicine("Bezoar"));
        medicines.add(new Medicine("Wiggenweld potion"));
        return medicines;
    }

    @GetMapping(path = "/receipt")
    public Receipt helloReceipt() throws TelegramApiException {
        return receiptService.saveTest();
    }
    @GetMapping(path = "/receipts")
    public List<Receipt> allReceipts() {

        List<Receipt> receipts = new ArrayList<>();
        receiptService.saveTest();
        receipts.add(new Receipt());
        receipts.add(new Receipt());
        return receipts;
    }


    @GetMapping(path = "/telegram")
    public org.telegram.telegrambots.meta.api.objects.Message telegramUser(@RequestParam String text) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        //sendMessage.set
        //new MedicalBot().execute(); CAACAgIAAxkBAAMsYgVNiVVx7CxMsKKNai_L984BaxcAAu1FAAKezgsAAY1pmsKRZvEZIwQ
        //Chat ID with Алена - 818477856
        SendSticker sendSticker = new SendSticker();
        sendSticker.setChatId("818477856");
        if(text.equals("sticker")) {
            sendSticker.setSticker(new InputFile("CAACAgIAAxkBAAMsYgVNiVVx7CxMsKKNai_L984BaxcAAu1FAAKezgsAAY1pmsKRZvEZIwQ"));
            return new MedicalBot().execute(sendSticker);
        }// sendSticker.setSticker("CAACAgIAAxkBAAMsYgVNiVVx7CxMsKKNai_L984BaxcAAu1FAAKezgsAAY1pmsKRZvEZIwQ");
        //sendMessage.setChatId("818477856");
        else {
            sendMessage.setText(text);
            sendMessage.setChatId("818477856");
        }
        return new MedicalBot().execute(sendMessage);
    }
    @PostMapping(path = "/{patientId}/sendASESTestEmail")
            public void sendEmail(@PathVariable UUID patientId, @RequestBody EmailBasicHeaders basicHeaders) throws MessagingException, DocumentException, IOException {
        log.info("Map to: {}", basicHeaders);
        //log.info("Email to: {}", basicHeaders.get("to"));

        emailService.sendSimpleMessageWithASESTest(String.valueOf(patientId), basicHeaders);
    }
}
