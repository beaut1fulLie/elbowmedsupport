package ru.etu.elbowsupport.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.etu.elbowsupport.models.Visit;
import ru.etu.elbowsupport.service.VisitService;

import javax.mail.MessagingException;
import java.util.UUID;

@RestController
@RequestMapping(path ="visit")
public class VisitController {

    @Autowired
    private VisitService visitService;

    @PostMapping(path = "/{id}/newVisit")
    public void saveVisit(@PathVariable UUID patientId,  @RequestBody Visit visit) {
        visitService.save(visit, String.valueOf(patientId));
    }
}
