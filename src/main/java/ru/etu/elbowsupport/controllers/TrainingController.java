package ru.etu.elbowsupport.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.etu.elbowsupport.models.Training;
import ru.etu.elbowsupport.service.PatientService;
import ru.etu.elbowsupport.service.TrainingService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path="/training")
public class TrainingController {


    @Autowired
    private TrainingService trainingService;

    @Autowired
    private PatientService patientService;

    @PostMapping(path = "/{patientId}")
    public Training saveTraining(@RequestBody Training training, @PathVariable UUID patientId) {
        return trainingService.save(training, String.valueOf(patientId));
    }

    @GetMapping(path = "/all/{patientId}")
    public List<Training> getAllTrainingsOfPatient(@PathVariable UUID patientId) {
        return patientService.getPatientById(String.valueOf(patientId)).getTrainings();
    }

}
