package ru.etu.elbowsupport;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import ru.etu.elbowsupport.enums.RolesEnum;
import ru.etu.elbowsupport.models.Role;
import ru.etu.elbowsupport.repository.RoleRepository;

@Configuration
@RequiredArgsConstructor
@Log4j2
public class OnAppStart {
    private final RoleRepository roleRepository;


    @EventListener(ApplicationReadyEvent.class)
    @org.springframework.core.annotation.Order(1)
    public void setRolesIfNot() {
        for (RolesEnum role : RolesEnum.values()) {
            if (!roleRepository.findByTitle(role).isPresent()) {
                log.info("Role " + role + " not found, inserting...");
                roleRepository.save(new Role(role));
            }
        }
    }
}