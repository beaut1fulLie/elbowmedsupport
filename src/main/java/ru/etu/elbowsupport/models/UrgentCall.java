package ru.etu.elbowsupport.models;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Data
@TypeDef(name = "list-array", typeClass = ListArrayType.class)
public class UrgentCall {
    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID callId;
    @Type(type="list-array")
    @Column(name="reasons", columnDefinition = "text[]")
    private List<String> reasons;
    private LocalDateTime dateOfCall;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    public UrgentCall(String[] reasons) {
        this.reasons =List.of(reasons);
    }
}
