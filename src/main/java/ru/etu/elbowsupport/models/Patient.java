package ru.etu.elbowsupport.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Transactional
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Patient extends User {

    //TODO Добавить рост,
    private String primaryASA;
    private int primaryBMI;
    private int age;
    private char gender;
    private LocalDateTime dateOfReplacement;

    private char side;
    private Integer medicalCardId;
    private String diagnosis;
    private String telegramChatId;
    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL /*, fetch= FetchType.LAZY*/)
    @JsonManagedReference
    private List<Receipt> receipts;
    @OneToMany(mappedBy = "patient")
    //@JsonBackReference
    //TODO Best suitable for now approach (JsonIgnoreProperties)
    @JsonIgnoreProperties("patient")
    private List<ASESTestResult> results;
    @OneToMany(mappedBy = "patient")
    @JsonIgnoreProperties("patient")
    private List<UrgentCall> calls;

    @OneToMany(mappedBy = "patient")
    @JsonIgnoreProperties("patient")
    private List<Training> trainings;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    @JsonIgnoreProperties("patients")
    private Doctor doctor;

    public Patient(int age, char gender, char side) {
        super();
    }

    public String getFullName() {
        return this.getLastname() + " " + this.getName();
    }

}
