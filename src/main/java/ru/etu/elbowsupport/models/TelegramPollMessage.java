package ru.etu.elbowsupport.models;

import lombok.ToString;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;

import java.io.Serializable;
import java.util.List;

public class TelegramPollMessage implements Serializable {

    public String chatId;
    public boolean isAnonymous;
    public List<String> options;
    public String question;
    public String message;

    public TelegramPollMessage(String chatId,
                               List<String> options, String question, String message) {
        this.chatId = chatId;
        this.isAnonymous = false;
        this.options = options;
        this.question = question;
        this.message = message;
    }
}
