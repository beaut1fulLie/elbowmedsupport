package ru.etu.elbowsupport.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
public class Training {
    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID trainingId;
    private String description;
    private LocalDateTime trainingDate;
    private int totalExercise;
    private int completedExercises;
    private boolean isCompleted = false;
    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;
}
