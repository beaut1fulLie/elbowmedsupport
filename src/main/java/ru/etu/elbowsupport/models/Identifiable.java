package ru.etu.elbowsupport.models;

import java.io.Serializable;

public interface Identifiable {
    Serializable getId();
}
