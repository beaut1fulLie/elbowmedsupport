package ru.etu.elbowsupport.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "ases_test_result")
@Getter
@Setter
public class ASESTestResult {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID resultId;
    private String totalResultText;
    private Integer totalSum;
    private Integer answeredQuestions;
    private boolean isCompleted;
    private LocalDateTime dateSent;
    private LocalDateTime dateCompleted;
    @ManyToOne
    @JoinColumn(name = "telegram_chat_id", referencedColumnName = "telegramChatId")
    //@JsonManagedReference
    private Patient patient;
    public ASESTestResult() {
        this.totalSum = 0;
        this.dateSent = LocalDateTime.now();
        this.answeredQuestions = 0;
        this.isCompleted = false;
    }
}

