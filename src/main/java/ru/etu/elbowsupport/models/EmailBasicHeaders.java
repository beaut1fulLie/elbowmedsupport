package ru.etu.elbowsupport.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class EmailBasicHeaders {
    private String to;
    private String subject;
    private String text;
}
