package ru.etu.elbowsupport.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@NoArgsConstructor
@Data
public class Doctor extends User {
    private Integer workExperience;
    private String post;

    @OneToMany(mappedBy = "doctor", fetch = FetchType.LAZY)
    //@JsonBackReference
    //TODO Best suitable for now approach (JsonIgnoreProperties)
    @JsonIgnoreProperties("doctor")
    private List<Patient> patients;

    public Doctor(String name, String lastname, int age) {
        super(name, lastname, age);
    }


}
