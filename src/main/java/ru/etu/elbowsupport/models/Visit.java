package ru.etu.elbowsupport.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.etu.elbowsupport.enums.ActivityStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;


@Entity
@NoArgsConstructor
@Data
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID visitId;
    private String reason;
    private LocalDateTime visitDate;
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;
    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;
}
