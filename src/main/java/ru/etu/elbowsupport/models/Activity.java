package ru.etu.elbowsupport.models;


import lombok.Data;
import lombok.NoArgsConstructor;
import ru.etu.elbowsupport.enums.ActivityStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Data
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID activityId;
    private String name;
    private String place;
    private Date date;
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;
}
