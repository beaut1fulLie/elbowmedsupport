package ru.etu.elbowsupport.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "ases_question")
@NoArgsConstructor
@Data
public class ASESQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID questionId;
    private String question;
}
