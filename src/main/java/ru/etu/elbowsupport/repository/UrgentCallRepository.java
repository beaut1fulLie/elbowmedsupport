package ru.etu.elbowsupport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.etu.elbowsupport.models.UrgentCall;

import java.util.UUID;

@Repository
public interface UrgentCallRepository extends JpaRepository<UrgentCall, UUID> {
}
