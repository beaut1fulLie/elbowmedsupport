package ru.etu.elbowsupport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.etu.elbowsupport.models.ASESTestResult;
import ru.etu.elbowsupport.models.Patient;

import java.util.List;
import java.util.UUID;

@Repository
public interface ASESTestResultRepository extends JpaRepository<ASESTestResult, UUID> {
    public List<ASESTestResult> findASESTestResultsByPatient(Patient patient);
    public ASESTestResult findASESTestResultByIsCompletedAndPatient(boolean isCompleted, Patient patient);
}
