package ru.etu.elbowsupport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.etu.elbowsupport.models.ASESQuestion;
import ru.etu.elbowsupport.models.ASESTestResult;

import java.util.UUID;

@Repository
public interface ASESQuestionRepository extends JpaRepository<ASESQuestion, UUID> {
}
