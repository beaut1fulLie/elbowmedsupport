package ru.etu.elbowsupport.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.etu.elbowsupport.enums.RolesEnum;
import ru.etu.elbowsupport.models.Role;

import java.util.Optional;
import java.util.UUID;
@Repository
public interface RoleRepository extends CrudRepository<Role, UUID> {
    Optional<Role> findByTitle(RolesEnum role);
}
