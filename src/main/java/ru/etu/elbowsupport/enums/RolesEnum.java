package ru.etu.elbowsupport.enums;

public enum RolesEnum {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_DOCTOR,
    ROLE_PATIENT
}
