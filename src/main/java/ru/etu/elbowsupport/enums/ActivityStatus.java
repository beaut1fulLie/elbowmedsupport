package ru.etu.elbowsupport.enums;

public enum ActivityStatus {
    NEXT,
    POSTPONED,
    CANCELLED,
    ENDED
}
