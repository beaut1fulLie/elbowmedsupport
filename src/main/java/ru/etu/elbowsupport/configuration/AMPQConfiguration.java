//package ru.etu.elbowsupport.configuration;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.core.Queue;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//@Slf4j
//public class AMPQConfiguration {
//    public static final String queueName = "poll-queue";
//
//    @Bean
//    public Queue getQueue(){
//        return new Queue(queueName, true, false, false);
//    }
//}
