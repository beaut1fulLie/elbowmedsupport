package ru.etu.elbowsupport.configuration.filter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Component
@RequiredArgsConstructor
@Slf4j
public class LogFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if(req.getMethod().equals(HttpMethod.OPTIONS.name())){
            chain.doFilter(request, response);
            return;
        }

        String vkId;
        try {
            String sign = URLDecoder.decode(req.getHeader("x-sign"), StandardCharsets.UTF_8.name());
            vkId = Arrays.stream(new URL(sign).getQuery().split("&"))
                    .filter(s->s.split("=")[0].equals("vk_user_id"))
                    .findFirst().orElseThrow(Exception::new).split("=")[1];
        } catch (Throwable ignored) {
            vkId = "error";
        }

        log.info("===========================================NEW REQUEST===========================================");
        log.info("method: {}, uri: {}, vkId: {}", req.getMethod(), req.getRequestURI(), vkId);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("User Details principal: {}" , authentication.getPrincipal());

        chain.doFilter(request, response);

        log.info("Response: cont.type: {}, status: {}", res.getContentType(), res.getStatus());
    }
}

