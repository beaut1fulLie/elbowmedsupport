package ru.etu.elbowsupport.configuration.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.etu.elbowsupport.models.User;

@Component
public class AuthenticationFacade {
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public User getPrincipal(){
        return (User) getAuthentication().getPrincipal();
    }
}