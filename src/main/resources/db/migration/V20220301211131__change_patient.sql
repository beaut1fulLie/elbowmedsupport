alter table patient DROP COLUMN age_of_death;
alter table patient ADD medical_card_id integer;
alter table patient ADD diagnosis varchar(255);