create table if not exists role
(
    title varchar(255) not null
        constraint role_pkey
            primary key
);
create table if not exists user_role
(
    user_id  uuid      not null
        constraint fkkjwgs4biy5rm8yexg7g8fs2ef
            references med_user
            on delete cascade,
    roles_title varchar(255) not null
        constraint fksx0qxr7t846qoc32m8se53jcn
            references role
);