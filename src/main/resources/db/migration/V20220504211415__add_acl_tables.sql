CREATE TABLE IF NOT EXISTS acl_sid (
id bigserial NOT NULL,
principal boolean NOT NULL,
sid varchar(100) NOT NULL,
PRIMARY KEY (id),
constraint unique_uk_1 UNIQUE   (sid,principal)
);

create table if not exists acl_class
(
    id            bigserial    not null
        constraint acl_class_pkey
            primary key,
    class         varchar(100) not null
        constraint unique_uk_2
            unique,
    class_id_type varchar(100)
);

create table if not exists acl_object_identity
(
    id                 bigserial   not null
        constraint acl_object_identity_pkey
            primary key,
    object_id_class    bigint      not null
        constraint foreign_fk_2
            references acl_class,
    object_id_identity varchar(36) not null,
    parent_object      bigint
        constraint foreign_fk_1
            references acl_object_identity,
    owner_sid          bigint
        constraint foreign_fk_3
            references acl_sid,
    entries_inheriting boolean     not null,
    constraint unique_uk_3
        unique (object_id_class, object_id_identity)
);

create table if not exists acl_entry
(
    id                  bigserial not null
        constraint acl_entry_pkey
            primary key,
    acl_object_identity bigint    not null
        constraint foreign_fk_4
            references acl_object_identity,
    ace_order           integer   not null,
    sid                 bigint    not null
        constraint foreign_fk_5
            references acl_sid,
    mask                integer   not null,
    granting            boolean   not null,
    audit_success       boolean   not null,
    audit_failure       boolean   not null,
    constraint unique_uk_4
        unique (acl_object_identity, ace_order)
);