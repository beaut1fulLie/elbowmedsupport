create table urgent_call
(
    call_id       uuid    not null
        constraint urgent_call_pkey
            primary key,
    reasons           text[],
    date_of_call  timestamp,
    lastname      varchar(255),
    patient_id   uuid
        constraint call_patient_fk
            references patient
);