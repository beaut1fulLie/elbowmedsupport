CREATE  TABLE med_user
(
    user_id       uuid    not null
        constraint med_user_pkey
            primary key,
    age           integer not null,
    created_when  timestamp,
    date_of_birth timestamp,
    lastname      varchar(255),
    login         varchar(255),
    name          varchar(255),
    password      varchar(255)
);
create table medical_facility
(
    medical_facility_id uuid not null
        constraint medical_facility_pkey
            primary key,
    city                varchar(255),
    country             varchar(255),
    name                varchar(255)
);
create table doctor
(
    user_id             uuid not null
        constraint doctor_pkey
            primary key
        constraint fki8mx6tqokpoyt5f76b9wajbcx
            references med_user,
    medical_facility_id uuid
        constraint fkqlbej739ox6elybhl6u06wyhb
            references medical_facility
);
create table patient
(
    age_of_death integer not null,
    gender       char    not null,
    primaryasa   varchar(255),
    primarybmi   integer not null,
    side         char    not null,
    user_id      uuid    not null
        constraint patient_pkey
            primary key
        constraint fklcdatyqnpj5wago97w0uwrjxl
            references med_user
);
create table medicine
(
    medicine_id      uuid    not null
        constraint medicine_pkey
            primary key,
    active_component varchar(255),
    amount           integer not null,
    name             varchar(255)
);

create table receipt
(
    receipt_id   uuid not null
        constraint receipt_pkey
            primary key,
    created_when timestamp,
    patient_id   uuid
        constraint fk2vp04mhy2doua0fv7se1oeh8l
            references patient
);
--
-- CREATE TABLE question
-- (
--     question_id       uuid    not null
--         constraint question_pkey
--             primary key,
--     question_text  varchar(255)
-- );
-- create table form_answer
-- (
--     answer_id uuid not null
--         constraint form_answer_pkey
--             primary key,
--     answer_text                varchar(255)
--
-- );
-- create table form
-- (
--     form_id             uuid not null
--         constraint form_pkey
--             primary key,
--     answer_id uuid
--         constraint form_answer_form_fk
--             references form_answer,
--     question_id uuid
--         constraint question_form_fk
--             references question
-- );
-- AlTER table patient ADD from_id uuid constraint form_patient_fk references form;
