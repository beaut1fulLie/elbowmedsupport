create table ases_question (
     question_id       uuid    not null
       constraint ases_question_pkey
       primary key,
     question   varchar(255) not null
);

create table ases_test_result (
    result_id       uuid    not null
        constraint ases_test_pkey
        primary key,
    total_sum   integer not null,
    total_result_text varchar(200) not null
)